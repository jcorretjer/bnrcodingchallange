package com.bignerdranch.android.blognerdranch.util

import com.bignerdranch.android.blognerdranch.util.dateformatter.DateFormat
import com.bignerdranch.android.blognerdranch.util.dateformatter.DateFormatter
import org.junit.Assert
import org.junit.Test
import org.junit.Assert.assertEquals

class DateFormatterTest {
    @Test
    fun formatDateWithEmptyCurrentFormatAndFailTest()
    {
        try
        {
            DateFormatter.format(DateFormat())

            throw Exception("Failed Test")
        }

        catch (e: Exception)
        {
            //If it catches an error than it passed because this is what we were expecting
            Assert.assertEquals(DateFormatter.ExceptionMessages.EMPTY_CURRENT_FORMAT, e.message)
        }
    }

    @Test
    fun formatDateWithEmptyOutputFormatAndFailTest()
    {
        try
        {
            DateFormatter.format(DateFormat(currentFormat = "yyyy-MM-dd HH:mm:ss"))

            throw Exception("Failed Test")
        }

        catch (e: Exception)
        {
            //If it catches an error than it passed because this is what we were expecting
            Assert.assertEquals(DateFormatter.ExceptionMessages.EMPTY_OUTPUT_FORMAT, e.message)
        }
    }

    @Test
    fun formatDateWithEmptyDateAndFailTest()
    {
        try
        {
            DateFormatter.format(DateFormat(currentFormat = "yyyy-MM-dd HH:mm:ss", outputFormat = "MMM dd, yyyy h:mm z"))

            throw Exception("Failed Test")
        }

        catch (e: Exception)
        {
            //If it catches an error than it passed because this is what we were expecting
            Assert.assertEquals(DateFormatter.ExceptionMessages.EMPTY_DATE, e.message)
        }
    }

    @Test
    fun formatDateWithCorrectArgumentsTest()
    {
        assertEquals("Jul 15, 2018 11:03", DateFormatter.format(DateFormat(currentFormat = "yyyy-MM-dd HH:mm:ss", outputFormat = "MMM dd, yyyy h:mm", date = "2018-07-15T23:03:29Z")))
    }
}