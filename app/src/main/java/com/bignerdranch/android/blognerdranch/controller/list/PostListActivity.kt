package com.bignerdranch.android.blognerdranch.controller.list

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewStub
import android.widget.Button
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.controller.data.helpers.BlogRetrofitHelper
import com.bignerdranch.android.blognerdranch.model.Post
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PostListActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_list)

        val notInternetViewStub = findViewById<ViewStub>(R.id.noInternetVwStub)

        val recyclerView = findViewById<RecyclerView>(R.id.post_recyclerview)

        val retrofitCallback = object : Callback<List<Post>>
        {
            override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>)
            {
                notInternetViewStub.visibility = View.GONE

                recyclerView.adapter = PostAdapter(response.body()!!)
            }

            override fun onFailure(call: Call<List<Post>>, t: Throwable)
            {
                notInternetViewStub.visibility = View.VISIBLE
            }
        }

        notInternetViewStub.inflate()

        findViewById<Button>(R.id.retryBtn).setOnClickListener{
            BlogRetrofitHelper.instance.getAllPosts().enqueue(retrofitCallback)
        }

        notInternetViewStub.visibility = View.GONE

        BlogRetrofitHelper.instance.getAllPosts().enqueue(retrofitCallback)
    }

    companion object {
        const val TAG = "PostListActivity"
    }
}
