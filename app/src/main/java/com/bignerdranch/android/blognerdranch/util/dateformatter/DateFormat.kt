package com.bignerdranch.android.blognerdranch.util.dateformatter

data class DateFormat(
    /**
     * Must be a valid date format based on SimpleDateFormat.
     */
    var outputFormat: String = "",

    /**
     * Must be a valid date format based on SimpleDateFormat.
     */
    var currentFormat: String = "",

    /**
     * Must be a valid date format based on SimpleDateFormat.
     */
    var date: String = ""
)
