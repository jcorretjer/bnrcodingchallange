package com.bignerdranch.android.blognerdranch.util.dateformatter

interface IDateFormatter
{
    fun format(dateFormat: DateFormat) : String
}