package com.bignerdranch.android.blognerdranch.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Metadata (
    @SerializedName("id")
    var postId: Int,
    var title: String,
    var summary: String,
    var author: Author,
    var publishDate: String
): Parcelable