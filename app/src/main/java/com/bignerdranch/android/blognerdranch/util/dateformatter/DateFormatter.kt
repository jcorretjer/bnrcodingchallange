package com.bignerdranch.android.blognerdranch.util.dateformatter

import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

object DateFormatter : IDateFormatter
{
    annotation class ExceptionMessages
    {
        companion object
        {
            const val EMPTY_CURRENT_FORMAT = "Current format must not be empty"
            const val EMPTY_OUTPUT_FORMAT = "Output format must not be empty"
            const val EMPTY_DATE = "Date must not be empty"
        }
    }

    override fun format(dateFormat: DateFormat): String
    {
        if(dateFormat.currentFormat.isEmpty())
        {
            throw Exception(ExceptionMessages.EMPTY_CURRENT_FORMAT)
        }

        if(dateFormat.outputFormat.isEmpty())
        {
            throw Exception(ExceptionMessages.EMPTY_OUTPUT_FORMAT)
        }

        if(dateFormat.date.isEmpty())
        {
            throw Exception(ExceptionMessages.EMPTY_DATE)
        }

        //Format with the pattern the date originally comes in
        val parser = SimpleDateFormat(dateFormat.currentFormat, Locale.getDefault())

        //The formatter kept throwing errors because of the 'T' and the 'Z'
        val dateFormatted = parser.parse(dateFormat.date.replace("T", " ").replace("Z", ""))

        //Format using the pattern we want
        return SimpleDateFormat(dateFormat.outputFormat, Locale.getDefault()).format(dateFormatted!!)
    }
}