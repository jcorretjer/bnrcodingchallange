package com.bignerdranch.android.blognerdranch.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Post (
    var id: Int,
    var metadata: Metadata,
    var body: String
): Parcelable