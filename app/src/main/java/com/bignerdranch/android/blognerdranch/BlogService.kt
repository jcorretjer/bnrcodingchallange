package com.bignerdranch.android.blognerdranch

import com.bignerdranch.android.blognerdranch.model.Post
import com.bignerdranch.android.blognerdranch.model.Metadata
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface BlogService {

    @GET("post-metadata")
    fun getPostMetadata(): Call<List<Metadata>>

    @GET("post/{id}")
    fun getPost(@Path("id") id: Int): Call<Post>

    @GET("post/all.json")
    fun getAllPosts(): Call<List<Post>>
}