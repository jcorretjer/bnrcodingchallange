package com.bignerdranch.android.blognerdranch.controller.list

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.controller.post.PostActivity
import com.bignerdranch.android.blognerdranch.model.Post
import com.bignerdranch.android.blognerdranch.util.dateformatter.DateFormat
import com.bignerdranch.android.blognerdranch.util.dateformatter.DateFormatter

class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

    private lateinit var post: Post

    private val titleTxtVw = itemView.findViewById<TextView>(R.id.titleTxtVw)

    private val authorTxtVw = itemView.findViewById<TextView>(R.id.authorTxtVw)

    private val summaryTxtVw = itemView.findViewById<TextView>(R.id.summaryTxtVw)

    private val publishedTxtVw = itemView.findViewById<TextView>(R.id.publishedTxtVw)

    init {
        itemView.setOnClickListener(this)
    }

    fun bind(post: Post)
    {
        this.post = post

        post.metadata.apply {
            titleTxtVw.text = title

            authorTxtVw.text = author.name ?: "Unknown author"

            summaryTxtVw.text = summary

            publishedTxtVw.text = DateFormatter.format(DateFormat(currentFormat = "yyyy-MM-dd HH:mm:ss", outputFormat = "MMM dd, yyyy h:mm", date = publishDate))
        }
    }

    override fun onClick(v: View) {
        val context = v.context
        context.startActivity(PostActivity.newIntent(v.context, post))
    }
}