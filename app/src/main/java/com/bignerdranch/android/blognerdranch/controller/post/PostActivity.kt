package com.bignerdranch.android.blognerdranch.controller.post

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toolbar
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.model.Post

class PostActivity : AppCompatActivity() {
    private var postTitle: TextView? = null
    private var postAuthor: TextView? = null
    private var postBody: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)

        postTitle = findViewById(R.id.title_textview)
        postAuthor = findViewById(R.id.author_textView)
        postBody = findViewById(R.id.body_textView)

        val post = intent.getParcelableExtra<Post>(POST_DATA)!!

        updateUI(post)
    }

    private fun updateUI(post: Post)
    {
        post.apply {
            postTitle?.text = metadata.title

            postAuthor?.text = metadata.author.name

            postBody?.text = body
        }

        findViewById<Toolbar>(R.id.detailsToolbar).apply {
            setActionBar(this)

            setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)

            setNavigationOnClickListener {
                onBackPressed()
            }
        }
    }

    companion object {
        const val TAG = "PostActivity"

        const val POST_DATA = "postData"

        fun newIntent(context: Context, post: Post): Intent {
            val intent = Intent(context, PostActivity::class.java)
            intent.putExtra(POST_DATA, post)
            return intent
        }
    }
}
