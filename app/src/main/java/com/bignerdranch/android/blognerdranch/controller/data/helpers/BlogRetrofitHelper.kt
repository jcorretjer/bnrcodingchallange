package com.bignerdranch.android.blognerdranch.controller.data.helpers

import com.bignerdranch.android.blognerdranch.BlogService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object BlogRetrofitHelper
{
    private const val BASE_URL = "http://10.0.2.2:8106/"

    val instance: BlogService = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(BlogService::class.java)
}