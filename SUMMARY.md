The way that I went about this was imagining myself in a scenario where we were a few days away from the deadline and the app was in an unstable state. I had to prioritize core functionalities and push the rest back for this initial release. My mission was to make the best with what I had without wasting too much time. My approach was similar to how game developers sometimes publish unpolished products just so they could meet their deadlines and later work on stabilizing them. 

***FYI, in my little story, the app we're making consists of a list of blogs, and when you select one, takes you to a detailed screen of that blog, and nothing more. It was pretty much done just needed some changes.***

The topics that I wanted to tackle were **UX/UI** and **minor code optimization**.

## UX/UI
Even though the original design served its purpose, some areas were difficult to navigate. Take for instance the post list, it was missing some key elements(title, summary, date, etc.), and it was hard to tell where an item started and where it ended. The detail screen was missing the return(back) button. A way to return to the previous screen is key to UX, even though the same can be achieved using the back button provided by the device, many users aren't aware of this. The UI is as plain as it can get, the least we could do is make it easy to use.

## Minor Code Optimization
Although performance issues were expected. The current state was unacceptable. It's bad enough that it's slow, but having it hang for a few extra seconds just wasn't going to do. By fetching the appropriate data set and maximize sharing data across screens, we increased our efficiency greatly.

### Bonus Feature - No internet connection
I had some extra time left over so I added a retry feature for when the app was launched without an internet connection. Previously, it seemed as if the app had stopped functioning because no data was displayed. Now it displays a message and a way to try again.

## Future changes

Refactor the current architecture into an MVVM pattern. Focusing on maintainability. The current code, although easy to read, maintaining it becomes exponentially more complex the more it grows.